#include <atomic>
#include <thread>
#include <iostream>

void StoreBuffering(size_t i) {
  // Shared
  std::atomic<int> x{0};
  std::atomic<int> y{0};

  // Local
  int r1 = 0;  // t1
  int r2 = 0;  // t2

  std::thread t1([&]() {
    x.store(1);
    r1 = y.load();
  });

  std::thread t2([&]() {
    y.store(1);
    r2 = x.load();
  });

  t1.join();
  t2.join();

  if (r1 == 0 && r2 == 0) {
    std::cout << "Iteration #" << i << ": Broken CPU!" << std::endl;
    std::abort();
  }
}

int main() {
  for (size_t i = 0; ; ++i) {
    StoreBuffering(i);
    if (i % 10000 == 0) {
      std::cout << "Iterations made: " << i + 1 << std::endl;
    }
  }
  return 0;
}
