#pragma once

#include <atomic>
#include <functional>
#include <mutex>

template <typename T>
class LazyValue {
  using Factory = std::function<T*()>;

 public:
  explicit LazyValue(Factory create)
      : create_(std::move(create)) {
  }

  T& Access() {
    // Double checked locking pattern
    T* curr_ptr = value_ptr_.load(/* memory order? */);
    if (curr_ptr == nullptr) {
      std::lock_guard<std::mutex> guard(mutex_);
      curr_ptr = value_ptr_.load(/* memory order? */);
      if (curr_ptr == nullptr) {
        curr_ptr = create_();
        value_ptr_.store(curr_ptr /*, memory order? */);
      }
    }
    return *curr_ptr;
  }

  ~LazyValue() {
    if (value_ptr_.load() != nullptr) {
      delete value_ptr_;
    }
  }

 private:
  Factory create_;
  std::mutex mutex_;
  std::atomic<T*> value_ptr_{nullptr};
};
